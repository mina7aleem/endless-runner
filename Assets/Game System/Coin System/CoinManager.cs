﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace CoinSystem
    {
        public class CoinManager : MonoBehaviour
        {
            private AudioSource audioSource;
            void Start()
            {
                audioSource = GetComponent<AudioSource>();
            }


            void OnTriggerEnter(Collider other) 
            {
                if (other.tag == "Player")
                {
                    audioSource.Play();
                    GetComponentInChildren<MeshRenderer>().enabled = false;
                    ScoringSystem.ScoreManager.score++;
                    Destroy(gameObject, 0.3f);
                }
            }
        }
    }
}


