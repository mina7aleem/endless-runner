﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace EnemySystem
    {
        public class EnemyBehavior : MonoBehaviour
        {
            public EnemyFireSystem.EnemyFireSpawner enemyFireSpawner;
            private int moveSpeed = 2; 
            float timer = 0f;
            [SerializeField] Transform firePoint;
            private Animator animator;
            void Start()
            {
                animator = GetComponent<Animator>();
            }

            void OnEnable() 
            {
                enemyFireSpawner = FindObjectOfType<EnemyFireSystem.EnemyFireSpawner>();

            }

            void Update()
            {
                MoveDirection();
                DeactivateEnemy();
                EnemyFire();
            }

            void MoveDirection()
            {
                transform.Translate(0, 0, moveSpeed * Time.deltaTime);
            }

            void OnCollisionEnter(Collision other) 
            {
                if (other.gameObject.tag == "PlayerFire")
                {
                    animator.SetTrigger("Die");
                }
            }

            void DeactivateEnemy()
            {
                if (transform.position.y < -1)
                {
                    gameObject.SetActive(false);
                }
            }

            IEnumerator WaitForDeactivate()
            {
                yield return new WaitForSeconds(4);
                gameObject.SetActive(false);
            }

            void EnemyFire()
            {
                timer += Time.deltaTime;
                if(timer > 1.5f)
                {
                    GameObject enemyFireBall = enemyFireSpawner.GetPooledFireBall();
                    enemyFireBall.transform.position = firePoint.position;
                    animator.SetTrigger("Attack");
                    enemyFireBall.SetActive(true);
                    timer = 0f;
                }
            }
        }
    }
}


