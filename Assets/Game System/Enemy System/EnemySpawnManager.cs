﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace EnemySystem
    {
        public class EnemySpawnManager : MonoBehaviour
        {
            private List<GameObject> enemyPool = new List<GameObject>();
            [SerializeField] GameObject enemy;
            private int enemyPoolNum = 5;
            float timer = 0f;
            [SerializeField] Transform playerPos;
            void Start()
            {
                FillingEnemyPool();                
            }
            

            void Update()
            {
                SpawnEnemy();
            }

            void FillingEnemyPool()
            {
                for (int i = 0; i < enemyPoolNum; i++)
                {
                    GameObject spawnedEnemy = Instantiate(enemy);
                    spawnedEnemy.SetActive(false);
                    enemyPool.Add(spawnedEnemy);
                }
            }

            public GameObject GetPooledEnemy()
            {
                for (int i = 0; i < enemyPool.Count; i++)
                {
                    if (!enemyPool[i].activeInHierarchy)
                    {
                        return enemyPool[i];
                    }
                }
                return null;
            }

            void SpawnEnemy()
            {
                 timer += Time.deltaTime;
                if(timer > 5f)
                {
                    int xPos = Random.Range(-4, 5);
                    GameObject spawnedEnem = GetPooledEnemy();
                    spawnedEnem.transform.position = new Vector3(xPos, 0.8f, playerPos.position.z + 45);
                    spawnedEnem.SetActive(true);
                    timer = 0f;
                } 
            }
        }
    }
}


