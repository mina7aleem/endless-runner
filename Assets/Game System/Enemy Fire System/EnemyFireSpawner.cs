using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace EnemyFireSystem
    {
        public class EnemyFireSpawner : MonoBehaviour
        {
            public List<GameObject> enemyFirePool = new List<GameObject>();
            [SerializeField] GameObject enemyFireBall;
            private int FireBallPoolNum = 20;
            void Start()
            {
                FillingFirePool();
            }

            void FillingFirePool()
            {
                for (int i = 0; i < FireBallPoolNum; i++)
                {
                    GameObject spawnedFireBall = Instantiate(enemyFireBall);
                    spawnedFireBall.SetActive(false);
                    enemyFirePool.Add(spawnedFireBall);
                }
            }

            public GameObject GetPooledFireBall()
            {
                for (int i = 0; i < enemyFirePool.Count; i++)
                {
                    if (!enemyFirePool[i].activeInHierarchy)
                    {
                        return enemyFirePool[i];
                    }
                }
                return null;
            }
        }
    }
}

