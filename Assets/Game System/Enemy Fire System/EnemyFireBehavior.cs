﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace EnemyFireSystem
    {
        public class EnemyFireBehavior : MonoBehaviour
        {
            private int moveSpeed = -60; 
            
            void OnEnable() 
            {
                StartCoroutine("WaitForDeactivate");
            }

            void Update()
            {
                MoveDirection();
            }

            void MoveDirection()
            {
                transform.Translate(0, 0, moveSpeed * Time.deltaTime);
            }

            void OnTriggerEnter(Collider other) 
            {
                if (other.tag == "Player" || other.tag == "PlayerFire")
                {
                    gameObject.SetActive(false);
                }
            }

            IEnumerator WaitForDeactivate()
            {
                yield return new WaitForSeconds(2);
                gameObject.SetActive(false);
            }
        }

    }
}

