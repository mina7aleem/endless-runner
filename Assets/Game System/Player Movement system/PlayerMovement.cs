﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace GameSystem
{
    namespace PlayerMovementsystem
    {
        public class PlayerMovement : MonoBehaviour
        {
            private float playerXPosition; 
            private int maxXPosition = 4;
            private int minXPosition = -4;
            private Rigidbody rB;
            private float forwardSpeed = 10f;
            private Animator animator;
            [SerializeField] AudioClip die;
            private AudioSource audioSource;
            void Start()
            {
                rB = GetComponent<Rigidbody>();
                animator = GetComponent<Animator>();
                audioSource = GetComponent<AudioSource>();
            }

            void Update()
            {
                MovePlayer();
            }

            void MovePlayer()
            {
                transform.Translate(0, 0, forwardSpeed * Time.deltaTime);
                playerXPosition = transform.position.x;
                if (Input.GetKeyDown(KeyCode.LeftArrow) && playerXPosition > minXPosition)
                {
                    animator.SetTrigger("RunLeft");
                    transform.Translate(-2, 0, 0);
                }
                if (Input.GetKeyDown(KeyCode.RightArrow) && playerXPosition < maxXPosition)
                {
                    animator.SetTrigger("RunRight");
                    transform.Translate(2, 0, 0);
                }                
            }

            IEnumerator OnTriggerEnter(Collider other) 
            {
                if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Saw" || other.gameObject.tag == "EnemyFire")
                {
                    animator.SetTrigger("Die");
                    audioSource.PlayOneShot(die);
                    gameObject.GetComponent<Collider>().enabled = false;
                    yield return new WaitForSeconds(1.5f);
                    Time.timeScale = 0;
                    SceneManager.LoadScene(2);
                }
            }

            void  WaitForSeconds()
            {
                StartCoroutine("OnTriggerEnter");
                
            }
        }
    }
}


