using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameSystem
{
    namespace UISystem
    {
        public class UIManager : MonoBehaviour
        {      
            [SerializeField] Scene gamePlay;
            public void StartGame()
            {
                SceneManager.LoadScene("GamePlay");
                ScoringSystem.ScoreManager.score = 0;
                Time.timeScale = 1; 
            }
            
            public void QuitGame()
            {
                Application.Quit();
                Debug.Log("QuitGame");
            }  
        }
    }
}