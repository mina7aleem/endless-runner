﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameSystem
{
    namespace AudioSystem
    {
        public class ButtonAudioManager : MonoBehaviour
        {
            private AudioSource audioSource;
            void Start()
            {
                audioSource = GetComponent<AudioSource>();
            }

            public void PlayButtonSound()
            {
                audioSource.Play();
            }
        }
    }
}


