using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace GroundSpawnSystem
    {
        public class GroundTile : MonoBehaviour
        {
            [SerializeField] GroundTilePoolManager groundTilePoolManager;
            [SerializeField] Transform groundTilePosition;
            private int coinNum = 20; 
            [SerializeField] GameObject coin;
            
            void Start()
            {
                SpawnCoin();
            }

            void Update()
            {
                
            }

            void OnTriggerExit(Collider other) 
            {
                if (other.tag == "Player")
                {
                    GameObject groundTile = groundTilePoolManager.GetPooledGroundTile();
                    groundTile.transform.position = groundTilePosition.position;
                    groundTile.SetActive(true);
                    gameObject.SetActive(false);
                }
            }

            void SpawnCoin()
            {
                for (int i = 0; i < coinNum; i++)
                {
                    GameObject spawnedCoin = Instantiate(coin);
                    Vector3 originalScale = spawnedCoin.transform.localScale;
                    spawnedCoin.transform.parent = gameObject.transform;
                    originalScale.Set(0.1f, 1, 0.005f);
                    spawnedCoin.transform.localScale = originalScale;      
                    int xPos = Random.Range(-4, 5);
                    int zPos = Random.Range(20, 140);
                    spawnedCoin.transform.position = new Vector3(xPos, 1.5f, zPos);
                }
            }
        }
    }
}


