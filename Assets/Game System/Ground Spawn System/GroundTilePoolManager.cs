using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace GroundSpawnSystem
    {
        public class GroundTilePoolManager : MonoBehaviour
        {
            private List<GameObject> groundTilesPool = new List<GameObject>();
            [SerializeField] GameObject groundTile;
            private int groundTilesPoolNum = 2;
            
            void Start()
            {
                FillingGroundTilePool();
            }

            void Update()
            {
                
            }

            void FillingGroundTilePool()
            {
                for (int i = 0; i < groundTilesPoolNum; i++)
                {
                    GameObject spawnedGroundTile = Instantiate(groundTile);
                    spawnedGroundTile.SetActive(false);
                    groundTilesPool.Add(spawnedGroundTile);
                }
            }

            public GameObject GetPooledGroundTile()
            {
                for (int i = 0; i < groundTilesPool.Count; i++)
                {
                    if (!groundTilesPool[i].activeInHierarchy)
                    {
                        return groundTilesPool[i];
                    }
                }
                return null;
            }
        }
    }
}


