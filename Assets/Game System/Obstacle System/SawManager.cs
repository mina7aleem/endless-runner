﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace ObstacleSystem
    {
        public class SawManager : MonoBehaviour
        {
            private float sawMoveSpeed = 4f;
            private Transform sawTransform;
            private float xPos;
            int chooseXDirection;
            int changeXDirection = 1; 
            
            void Start()
            {
                sawTransform = GetComponent<Transform>();
                xPos = sawTransform.position.x;
                chooseXDirection = Random.Range(-1, 2);
            }

            void Update()
            {
                MoveSaw();
            }

            private void MoveSaw()
            {
                if (xPos > 0 && xPos < 5)
                {
                    transform.Translate(-changeXDirection * sawMoveSpeed * Time.deltaTime, 0, 0);
                    return;
                }
                if (xPos < 0 && xPos > -5)
                {
                    transform.Translate(changeXDirection * sawMoveSpeed * Time.deltaTime, 0, 0);
                    return;
                }
                else
                {
                    transform.Translate(changeXDirection * chooseXDirection * sawMoveSpeed * Time.deltaTime, 0, 0);
                }
            }

            void OnTriggerExit(Collider other) 
            {
                if (other.gameObject.tag == "SawBoundries")
                {
                    changeXDirection = -changeXDirection;
                }
            }
        }
    }
}

