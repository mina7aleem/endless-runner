﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GameSystem
{
    namespace PlayerFireSystem
    {
        public class FireBallBehavior : MonoBehaviour
        {
            private int moveSpeed = 40; 
            void Start()
            {
                
            }

            void OnEnable() 
            {
                StartCoroutine("WaitForDeactivate");
            }

            void Update()
            {
                MoveDirection();
            }

            void MoveDirection()
            {
                transform.Translate(0, 0, moveSpeed * Time.deltaTime);
            }

            void OnTriggerEnter(Collider other) 
            {
                if (other.tag == "Enemy")
                {
                    gameObject.SetActive(false);
                }
            }

            IEnumerator WaitForDeactivate()
            {
                yield return new WaitForSeconds(4);
                gameObject.SetActive(false);
            }
        }
    }
}

