﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem
{
    namespace PlayerFireSystem
    {
        public class PlayerFireManager : MonoBehaviour
        {
            private List<GameObject> FirePool = new List<GameObject>();
            [SerializeField] GameObject FireBall;
            private int FireBallPoolNum = 20;
            [SerializeField] Transform firePoint;
            [SerializeField] AudioClip fire;
            private AudioSource audioSource;
            void Start()
            {
                FillingFirePool();
                audioSource = GetComponent<AudioSource>();
            }

            void Update()
            {
                PlayerFire();
            }

            void FillingFirePool()
            {
                for (int i = 0; i < FireBallPoolNum; i++)
                {
                    GameObject spawnedFireBall = Instantiate(FireBall);
                    spawnedFireBall.SetActive(false);
                    FirePool.Add(spawnedFireBall);
                }
            }

            public GameObject GetPooledFireBall()
            {
                for (int i = 0; i < FirePool.Count; i++)
                {
                    if (!FirePool[i].activeInHierarchy)
                    {
                        return FirePool[i];
                    }
                }
                return null;
            }

            void PlayerFire()
            {
                if (gameObject.GetComponent<Collider>().enabled == true)
                {
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        GameObject fireBall = GetPooledFireBall();
                        fireBall.transform.position = firePoint.position;
                        fireBall.SetActive(true);
                        audioSource.PlayOneShot(fire);
                    }
                }
                
            }
        }
    }
}